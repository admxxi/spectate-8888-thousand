var lottery = (function () {
  'use strict';

  const _getNextDay = (day_in_week, date) => {
    let currentDate = date instanceof Date ? new Date(date.getTime()) : new Date();
    let dayInWeek = day_in_week || 0;

    let result = currentDate.getDate() + ((dayInWeek - 1 - currentDate.getDay() + 7) % 7);

    currentDate.setDate(result);
    currentDate.setHours(20);
    currentDate.setMinutes(0);
    currentDate.setSeconds(0);

    return currentDate;
  };

  const _WednesdayOrSunday = (date) => {
    let currentDate = date instanceof Date ? new Date(date.getTime()) : new Date();
    let nextWednesday = _getNextDay(4, currentDate);
    let nextSunday = _getNextDay(1, currentDate);
    let dates = [nextSunday.getTime(), nextWednesday.getTime()];

    let onlyFutureDates = dates.filter((singleDate) => {
      return singleDate > currentDate;
    });

    let onlyFutureDatesOrderByDate = onlyFutureDates.sort(function (a, b) {
      return a - b;
    });

    if (!onlyFutureDatesOrderByDate.length) {
      throw 'Could not find next draw.';
    }

    return new Date(onlyFutureDatesOrderByDate[0]);
  };

  const nextDraw = (date) => {
    try {
      return "Lottery's next draw will be on " + _WednesdayOrSunday(date).toLocaleString();
    } catch (err) {
      console.error(err);
      return err.toString();
    }
  };

  return {
    nextDraw,
  };
})();
