window.onload = () => {
  (() => {
    'use strict';

    let flip = document.getElementsByClassName('animated__flip');
    let cards = document.getElementsByClassName('animated__flip__inner__card');
    let cta = document.getElementsByClassName('cta');

    const fixHeight = () => {
      let childrenHeight = 0;

      for (let i = 0; i < cards.length; i++) {
        let card = cards[i];

        if (card && card.children.length) {
          if (card.children[0].offsetHeight > childrenHeight) {
            childrenHeight = card.children[0].offsetHeight;
            card.parentNode.style.minHeight = card.style.minHeight = childrenHeight + 'px';
          }
        }
      }
    };

    const flipClass = () => {
      let flipInner = flip[0].getElementsByClassName('animated__flip__inner');

      if (flipInner.length) {
        flipInner[0].classList.toggle('animated__flip__inner--turned');
      }

      if (cards.length === 2) {
        setTimeout(() => {
          cards[0].classList.toggle('hide');
          cards[1].classList.toggle('hide');
          fixHeight();
        }, 100);
      }
    };

    if (flip.length) {
      let flipAction = document.getElementsByClassName('flip-action');

      for (let i = 0; i < flipAction.length; i++) {
        flipAction[i].addEventListener('click', flipClass);
      }
    }

    if (cta.length) {
      const showNextDraw = () => {
        try {
          alert(lottery.nextDraw());
        } catch (err) {
          console.error(err);
        }
      };

      for (let i = 0; i < cta.length; i++) {
        cta[i].addEventListener('click', showNextDraw);
      }
    }

    window.addEventListener('resize', function () {
      fixHeight();
    });

    fixHeight();
  })();
};
